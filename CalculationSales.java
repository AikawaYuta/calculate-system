package jp.alhinc.aikawa_yuta.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;


public class CalculationSales {
	/**
	 * @param args
	 */
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException{
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}


		HashMap<String, String> branchMap = new HashMap<String, String>();//マップ(支店)の変数宣言
		HashMap<String, Long> salesMap = new HashMap<String, Long>();//マップ(売上)の変数宣言

		if(fileRead(branchMap,salesMap,args[0],"branch.lst") == false){
			return;
		}
        //branchファイル読み込みメソッドの呼び出し


        /*売上ファイルの読み込み*/
        ArrayList<File> rcdFiles = new ArrayList<File>();

			File file =new File(args[0]);
			File files[] = file.listFiles();
			for(int i = 0; i < files.length;i++){
				if(files[i].getName().matches("^\\d{8}.rcd$") && files[i].isFile()){
					rcdFiles.add(files[i]);
				}
			}


			//連番チェック
			for(int s = 0; s < rcdFiles.size()-1; s++ ){
				String getFirst = rcdFiles.get(s).getName();
				String gotFirst = getFirst.substring(0,getFirst.lastIndexOf("."));
				int firstList = Integer.parseInt(gotFirst);
				String getSecond = rcdFiles.get(s + 1).getName();
				String gotSecond = getSecond.substring(0,getSecond.lastIndexOf("."));
				int secondList = Integer.parseInt(gotSecond);
				if(secondList - firstList != 1){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}


		BufferedReader br = null;
		try{
			for(int n = 0; n < rcdFiles.size(); n++){
				ArrayList<String> salesList = new ArrayList<String>();
				FileReader fr = new FileReader(rcdFiles.get(n));
				br = new BufferedReader(fr);
				String line;
				int lineCount = 0;
				while((line = br.readLine()) != null){
					lineCount++;
					salesList.add(line);
				}
				if(lineCount != 2){
					System.out.println(rcdFiles.get(n).getName() + "のフォーマットが不正です");
					return;
			    }
				//売上金額の桁数チェック
				if(!salesList.get(1).matches("\\d{1,10}")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//売上ファイルの支店コードチェック
				if(!branchMap.containsKey(salesList.get(0))){
			    	System.out.println(rcdFiles.get(n).getName()+ "の支店コードが不正です");
					return;
				}
				long total = salesMap.get(salesList.get(0));
				total += Long.parseLong(salesList.get(1));
				if(String.valueOf(total).length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				salesMap.put(salesList.get(0), total);
			}
		}
		catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		finally{
			if(br != null){
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		if(outPut(branchMap, salesMap, args[0],"branch.out") == false){
			return;
		}
		//出力ファイルのメソッド呼び出し


	}
	public static boolean fileRead(HashMap<String, String> nameMap,HashMap<String, Long> salesMap,String dirPath,String filereadName){
		BufferedReader br = null;
		try{
			File file = new File(dirPath, filereadName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[]data = line.split(",",0);//行をカンマ区切りで配列に変換
				if(data.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				long n = 0;
				if(data[1].matches("\\,")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				else if(data[0].matches("\\d{3}")) {
					nameMap.put(data[0], data[1]);//支店マップにデータを格納;
					salesMap.put(data[0], n); //売上マップを用意
				}
				else {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
			}
		}
		catch(IOException e) {
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}
		finally {
			if(br != null){
				try{
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}



	public static boolean outPut(HashMap<String, String> nameMap,HashMap<String, Long> salesMap,String dirPath,String fileoutName) {
		BufferedWriter bw = null;
		try{
			File outFile = new File(dirPath,fileoutName);
			FileWriter fw = new FileWriter(outFile);
			bw = new BufferedWriter(fw);
			for (Entry<String, String>branchEntry : nameMap.entrySet()){
				bw.write(branchEntry.getKey() +  "," + branchEntry.getValue() + "," + salesMap.get(branchEntry.getKey()));
				bw.newLine();
				//System.out.println(branchMap);
			}


		}
		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally{
			if(bw != null){
				try {
					bw.close();
				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
